DOC_NAME = Thesis

# I don't have a separate references directory
REFERENCES_DIR =

# My styles are all in the root
STYLES_DIR =

# also no graphics
GRAPHICS_DIR =
 
include mk/platform.mk
include mk/targets.mk
