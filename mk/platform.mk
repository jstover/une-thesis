ifeq ($(OS),Windows_NT)
CP  := copy
RM    := del /Q /F
RMDIR := rmdir /S /Q
FIND  := mk\bin\find.exe
TAR   := mk\bin\bsdtar.exe
else
CP  := cp -a
RM    := rm -rf
RMDIR := rm -rf
FIND  := find
TAR   := tar
endif
