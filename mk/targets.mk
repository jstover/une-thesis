###
### Makefile fragment for building LaTeX documents
###

#! Name of the master .tex file to use (TODO: strip .tex extension)
DOC_NAME 		?= Thesis

#! Filename for the output PDF file
PDF   			?= $(DOC_NAME).pdf

#! Filename for the compressed archive (TODO: support more than tar.gz)
TARGZ 			?= $(DOC_NAME).tar.gz

## Path to the directory containing graphics files
GRAPHICS_DIR  	?= graphics

## Path to the directory containing .bib reference files
REFERENCES_DIR 	?= references

## Path to the directory containing the document's content files
CHAPTERS_DIR 	?= chapters

## Path to the directory containing the preamble files (title/cover/ack)
PREAMBLE_DIR    ?= preamble

## Path to the directory containing the appendix files
APPENDIX_DIR 	?= appendices

## Path to the directory containing LaTeX .sty files
STYLES_DIR  	?= styles

## Path to the project's README file (if present)
README          ?= $(wildcard readme*) $(wildcard README*) $(wildcard Readme*)

## List of all .tex files for the project. Only override this if you know what you're doing
TEX_FILES       ?= $(wildcard ./*.tex) $(wildcard ${PREAMBLE_DIR}/*.tex) $(wildcard ${APPENDIX_DIR}/*.tex)

## List of all .bib files for the project
BIB_FILES       ?= $(wildcard ./$(REFERENCES_DIR)/*.bib)

## Path to all .sty files for the project
STY_FILES  		?= $(wildcard ./$(STYLES_DIR)/*.sty)

## Name of the executable to use for compiling latex (.tex) files
LATEX 			?= xelatex

## Name of the executable to use for compiling biblatex (.bib) files
BIBLATEX 		?= biber

## Path to the directory containing Makefile fragments (including this one!)
MK ?= $(wildcard ./mk)

## Name of the directory to use for the 'build' target
BUILD_DIR  ?= build

## List of extra file extensions to remove with the 'clean' target
EXTRA_BUILD_EXTS ?= 

BUILD_EXTS := out log lot lof run.xml blg bbl ist toc glo glg gls soc aux dvi ps pdf tar.gz bcf xdy glsdefs $(EXTRA_BUILD_EXTS)

TMP_FILES =  $(wildcard $(foreach var,$(BUILD_EXTS),*.$(var)))
TMP_FILES += $(wildcard $(foreach var,-blx.bib,*$(var)))
TMP_FILES += $(wildcard ./$(GRAPHICS_DIR)/**/*-converted-to.pdf)
TMP_FILES += $(wildcard $(BUILD_DIR))

CLEAN_FILES ?= $(strip $(TMP_FILES))

AUX=$(DOC_NAME).aux
BBL=$(DOC_NAME).bbl
GLS=$(DOC_NAME).gls
BCF=$(DOC_NAME).bcf

make_header = "========== MAKE $(1): ($(2))"

.PHONY: default pdf tar debug clean build pre-build

default: build

pdf: $(PDF)

tar: $(GRAPHICS_DIR) $(REFERENCES_DIR) $(CHAPTERS_DIR) $(PREAMBLE_DIR) $(APPENDIX_DIR) $(TEX_FILES) $(BIB_FILES) $(STY_FILES) $(README) $(MK) Makefile
	@echo $(call make_header,"tar",$(TARGZ))
	$(TAR) --exclude="._*" --exclude='*.swp' -cvzf $(TARGZ) $^

debug:
	@echo "README=($(README))"
	@echo "GRAPHICS_DIR=($(GRAPHICS_DIR))"
	@echo "REFERENCES_DIR=($(REFERENCES_DIR))"
	@echo "CHAPTERS_DIR=($(CHAPTERS_DIR))"
	@echo "PREAMBLE_DIR=($(PREAMBLE_DIR))"
	@echo "APPENDIX_DIR=($(APPENDIX_DIR))"
	@echo "STYLES_DIR=$((STYLES_DIR))"
	@echo "TEX_FILES=($(TEX_FILES))"
	@echo "BIB_FILES=($(BIB_FILES))"
	@echo "BUILD_EXTS=($(BUILD_EXTS))"
	@echo "CLEAN_FILES=($(CLEAN_FILES))"
	@echo "MAKE=($(MAKE))"
	echo $(call make_header,"BCF", $(BCF))

clean:
ifneq ($(CLEAN_FILES),)
	$(RM) $(subst /,\,$(foreach f,$(CLEAN_FILES),"$(f)"))
endif

build: tar
	[ -d build ] || mkdir $(BUILD_DIR)
	cd $(BUILD_DIR) && $(TAR) xzf ../$(TARGZ) && $(MAKE) pdf && $(CP) *.pdf ../


$(PDF): $(BCF) $(AUX) $(BBL) $(GLS) pre-build
	@echo $(call make_header,"pdf",$(PDF))
# I don't have a good way to do this on Windows yet, so the build order must be defined here
ifeq ($(OS),Windows_NT)
	$(LATEX) $(DOC_NAME)
	$(BIBLATEX) $(DOC_NAME)
	makeglossaries $(DOC_NAME)
	$(LATEX) $(DOC_NAME)
	$(LATEX) $(DOC_NAME)
else
	while ($(LATEX) $(DOC_NAME) && grep -i -q "rerun to get\|rerun $(BIBLATEX)\|rerun LaTex\|\(re\)run $(BIBLATEX)" $(DOC_NAME).log); do echo "===== rerun"; done
endif


%.bcf: $(BIB_FILES) $(TEX_FILES)
	@echo $(call make_header,"bcf",$(BCF))
	$(LATEX) $(DOC_NAME)

%.aux: $(BIB_FILES) $(TEX_FILES)
	@echo $(call make_header,"aux",$(AUX))
	$(LATEX) $(DOC_NAME)

%.gls: $(AUX) $(TEX_FILES)
	@echo $(call make_header,"gls",$(GLS))
	makeglossaries $(DOC_NAME)

%.bbl: $(AUX) $(BIB_FILES)
	@echo $(call make_header,"bbl",$(BBL))
	$(BIBLATEX) $(DOC_NAME)
	
